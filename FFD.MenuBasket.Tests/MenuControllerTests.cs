using FFD.MenuBasket.Api.Controllers;
using FFD.MenuBasket.DataAccess.Repositories;
using FFD.MenuBasket.Domain.Entities;
using FFD.MenuBasket.Domain.SubModels;

namespace FFD.MenuBasket.Tests
{
    public class MenuControllerTests
    {
        private readonly Mock<IRepository<Dish>> _menuRepoMock;
        private readonly Mock<ILogger<MenuController>> _loggerMock;
        private readonly MenuController _controller;

        public MenuControllerTests()
        {
            _menuRepoMock = new Mock<IRepository<Dish>>();
            _loggerMock = new Mock<ILogger<MenuController>>();
            _controller = new MenuController(_menuRepoMock.Object, _loggerMock.Object);
        }

        [Fact]
        public async Task AddNewDishAsync_ShouldReturnOkResult()
        {
            // Arrange
            var request = new CreateOrEditDishRequest
            {
                Name = "Test Dish",
                Price = 10,
                Description = "Test Description",
                Category = "Test Category"
            };
            var dish = new Dish
            {
                Name = request.Name,
                Price = request.Price,
                Description = request.Description,
                Category = request.Category
            };

            _menuRepoMock.Setup(repo => repo.AddNewDishAsync(It.IsAny<Dish>()))
                .ReturnsAsync(1); //��� ����. ������ AddNewDishAsync � ����� ���������� Dish ������ Task � ����������� 1

            // Act
            var result = await _controller.AddNewDishAsync(request);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal(1, okResult.Value);
            _menuRepoMock.Verify(repo => repo.AddNewDishAsync(It.IsAny<Dish>()),
                Times.Once); //���������, ��� ����� AddNewDishAsync ��� ������ ����� ���� ���

            //�������� ������������  - ���� ����������� (������ �� ��������)

            //_loggerMock.Verify(logger => logger.Log(
            //        LogLevel.Critical,
            //        It.IsAny<EventId>(),
            //        It.Is<It.IsAnyType>((o, t) => o.ToString().Contains("Post")),
            //        null,
            //        null!),
            //    Times.Once);
        }

        [Fact]
        public async Task GetByIdAsync_ShouldReturnOkResult_WithDish()
        {
            // Arrange
            int dishId = 1;
            var dish = new Dish
            {
                Id = dishId,
                Name = "Test Dish",
                Price = 10,
                Description = "Test Description",
                Category = "Test Category"
            };

            _menuRepoMock.Setup(repo => repo.GetByIdAsync(dishId))
                .ReturnsAsync(dish);

            // Act
            var result = await _controller.GetByIdAsync(dishId);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var returnedDish = Assert.IsType<Dish>(okResult.Value);
            Assert.Equal(dishId, returnedDish.Id);
            _menuRepoMock.Verify(repo => repo.GetByIdAsync(dishId), Times.Once);
        }

        [Fact]
        public async Task GetAllAsync_ShouldReturnOkResult_WithListOfDishes()
        {
            // Arrange
            var dishes = new List<Dish>
            {
                new Dish
                {
                    Id = 1, Name = "Dish 1", Price = 10, Description = "Description 1", Category = "Category 1"
                },
                new Dish { Id = 2, Name = "Dish 2", Price = 11, Description = "Description 2", Category = "Category 2" }
            };

            _menuRepoMock.Setup(repo => repo.GetAllAsync())
                .ReturnsAsync(dishes);

            // Act
            var result = await _controller.GetAllAsync();

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var returnedDishes = Assert.IsType<List<Dish>>(okResult.Value);
            Assert.Equal(2, returnedDishes.Count);
            _menuRepoMock.Verify(repo => repo.GetAllAsync(), Times.Once);
        }

        [Fact]
        public async Task DeleteDish_ShouldReturnOkResult_WhenDishDeleted()
        {
            // Arrange
            int dishId = 1;
            _menuRepoMock.Setup(repo => repo.DeleteDishByIdAsync(dishId))
                .ReturnsAsync(true);

            // Act
            var result = await _controller.DeleteDish(dishId);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal($"Dish � id = {dishId} ������.", okResult.Value);
            _menuRepoMock.Verify(repo => repo.DeleteDishByIdAsync(dishId), Times.Once);
        }

        [Fact]
        public async Task DeleteDish_ShouldReturnNotFound_WhenDishNotDeleted()
        {
            // Arrange
            int dishId = 1;
            _menuRepoMock.Setup(repo => repo.DeleteDishByIdAsync(dishId))
                .ReturnsAsync(false);

            // Act
            var result = await _controller.DeleteDish(dishId);

            // Assert
            var notFoundResult = Assert.IsType<NotFoundObjectResult>(result);
            Assert.Equal(dishId, notFoundResult.Value);
            _menuRepoMock.Verify(repo => repo.DeleteDishByIdAsync(dishId), Times.Once);
        }

        [Fact]
        public async Task UdateByIdAsync_ShouldReturnOkResult_WithNewDishId()
        {
            // Arrange
            int dishId = 1;
            var request = new CreateOrEditDishRequest
            {
                Name = "Updated Dish",
                Price = 10,
                Description = "Updated Description",
                Category = "Updated Category"
            };

            _menuRepoMock.Setup(repo => repo.DeleteDishByIdAsync(dishId))
                .ReturnsAsync(true);

            _menuRepoMock.Setup(repo => repo.AddNewDishAsync(It.IsAny<Dish>()))
                .ReturnsAsync(dishId);

            // Act
            var result = await _controller.UdateByIdAsync(dishId, request);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal(dishId, okResult.Value);
            _menuRepoMock.Verify(repo => repo.DeleteDishByIdAsync(dishId), Times.Once);
            _menuRepoMock.Verify(repo => repo.AddNewDishAsync(It.IsAny<Dish>()), Times.Once);
        }

        [Fact]
        public async Task UdateByIdAsync_ShouldReturnNotFound_WhenDishNotDeleted()
        {
            // Arrange
            int dishId = 1;
            var request = new CreateOrEditDishRequest
            {
                Name = "Updated Dish",
                Price = 10,
                Description = "Updated Description",
                Category = "Updated Category"
            };

            _menuRepoMock.Setup(repo => repo.DeleteDishByIdAsync(dishId))
                .ReturnsAsync(false);

            // Act
            var result = await _controller.UdateByIdAsync(dishId, request);

            // Assert
            var notFoundResult = Assert.IsType<NotFoundObjectResult>(result);
            Assert.Equal(dishId, notFoundResult.Value);
            _menuRepoMock.Verify(repo => repo.DeleteDishByIdAsync(dishId), Times.Once);
            _menuRepoMock.Verify(repo => repo.AddNewDishAsync(It.IsAny<Dish>()), Times.Never);
        }
    }
}
