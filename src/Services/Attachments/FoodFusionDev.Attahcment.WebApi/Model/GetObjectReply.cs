﻿using Minio.DataModel;

namespace FoodFusionDev.Attahcment.WebApi.Model
{
    public class GetObjectReply
    {
        public ObjectStat objectstat { get; set; }
        public byte[] data { get; set; }
    }
}
