﻿namespace FoodFusionDev.Attahcment.WebApi.Model
{
    public class UploadRequest
    {
        public UploadTypeList type { get; set; }
        public byte[] data { get; set; }
    }
}
