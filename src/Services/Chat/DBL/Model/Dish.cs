﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace FFD.Chat.DBL.Model
{
    public class Dish
    {
        //[Key, Column(Order = 0)]
        //public int Id { get; set; }
        [Key, Column(Order = 0)]
        public int OrderId { get; set; }
        [Key, Column(Order = 1)]
        public int DishId { get; set; }

        public int DishImg { get; set; }

        public string DishName { get; set; }

        public int DishPrice { get; set; }

        public string DishDescpription { get; set; }

        public int DishQuantity { get; set; }

        public string Status { get; set; }

    }
}
