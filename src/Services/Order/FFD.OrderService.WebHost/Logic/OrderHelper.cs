﻿using FFD.OrderService.Core.Entities;
using FFD.OrderService.DataAccess;
using FFD.OrderService.DataAccess.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Data;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace FFD.OrderService.WebHost.Logic
{
    public class OrderHelper
    {
        private readonly IUnitOfWork _unitOfWork;

        public OrderHelper(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public async Task<Order> CreateAsync(Guid clientId, OrderDetail[] items) //double total,
        {
            //check client 
            var client = await _unitOfWork.GetRepository<Client>()
                               .GetByIdAsync(clientId) 
                               ?? throw new Exception($"Client with id={clientId} not found");

            var total = items.Sum(d => d.Total);

            var orderCreation = DateTime.Now.ToUniversalTime();
            var order = new Order()
            {
                ClientId = clientId,
                Date = orderCreation,
                State = OrderStateEnum.Open,
                StateDate = orderCreation,
                Total = total
            };

            //add order to DB
            using (var readTransaction = await _unitOfWork.Context.Database.BeginTransactionAsync(IsolationLevel.Serializable))
            {
                order.Number = GetOrderNumber(order.Date.Year);

                //add order
                await _unitOfWork.GetRepository<Order>().AddAsync(order);

                //add order details
                foreach (var i in items) 
                    i.OrderId = order.Id;

                await _unitOfWork.GetRepository<OrderDetail>().AddRangeAsync(items); 

                //add history
                await AddOrderHistoryAsync(order);

                //commit
                await readTransaction.CommitAsync(); 
            }

            return order;
        }

        //public async Task AddItemsAsync(List<OrderDetail> items )
        //{
        //    if (items.Count == 0) return;

        //   // var orderId = items[0].Id;
        //    var odRepo = _unitOfWork.GetRepository<OrderDetail>();

        //    //using (var odTransaction = await _unitOfWork.Context.Database.BeginTransactionAsync(IsolationLevel.Serializable))
        //    //{
        //        //var orderItems = await odRepo.GetByIdAsync(orderId);
        //        odRepo.AddRangeAsync(items.ToArray());
        //        //orderItems.
        //        //double total = (await _orderDetailRepo.FindAllByAsync(d => d.OrderId == request.OrderId)).Sum(d => d.Total);

        //        ////find position in order if exist
        //        //var orderDetail = (await _orderDetailRepo.FindAllByAsync(d => d.OrderId == request.OrderId &&
        //        //                                                              d.DishId == request.DishId))
        //        //                                         .FirstOrDefault();

        //        //if (orderDetail == null)
        //        //    orderDetail = new OrderDetail();
        //        //else total -= orderDetail.Total;

        //        //orderDetail.Count = request.Count;
        //        //orderDetail.Discount = request.Discount;
        //        //orderDetail.Price = request.Price;

        //        //if (orderDetail.OrderId == Guid.Empty)
        //        //{
        //        //    orderDetail.OrderId = request.OrderId;
        //        //    orderDetail.DishId = request.DishId;
        //        //    await _orderDetailRepo.AddAsync(orderDetail); //saveChanges
        //        //}
        //        //else
        //        //    await _orderDetailRepo.SaveChangesAsync();

        //        ////update Orders.total
        //        //order.Total = total + orderDetail.Total;
        //        //await _orderRepository.SaveChangesAsync();

        //        //commit
        //   //     await odTransaction.CommitAsync();
        //   // }

        //}

        /// <summary>
        /// Изменение статуса заказа
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="newState"></param>
        /// <returns></returns>
        public async Task<Order> ChangeOrderStateAsync(Guid orderId, OrderStateEnum newState)
        {            
            var orderRepo = _unitOfWork.GetRepository<Order>();

            //get order
            var order = await orderRepo.GetByIdAsync(orderId);
            if (order == null) 
                throw new Exception($"Order id={orderId} not found");

            //set state
            order.State = newState;
            order.StateDate = DateTime.Now.ToUniversalTime(); 

            await orderRepo.SaveChangesAsync();

            return order;
        }

        /// <summary>
        /// Добавление записи изменения статуса заказа в историю
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public async Task<OrderHistory> AddOrderHistoryAsync(Order order)
        {
            var orderHistory = new OrderHistory()
            {
                OrderId = order.Id,
                State = order.State,
                StateDate = order.StateDate
            };

            await _unitOfWork.GetRepository<OrderHistory>().AddAsync(orderHistory);//_historyRepository

            return orderHistory;
        }
        private string GetOrderNumber(int year)
        {
            return (_unitOfWork.Context.Set<Order>()
                                       .Count(o => o.Date.Year == year) + 1)
                                       .ToString();//_context.Set<Order>().Where(o => o.Date.Year == year).Select(o=>o.Number).DefaultIfEmpty().Max();//o => o.Number
        }
    }
}