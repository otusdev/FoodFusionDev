﻿namespace FFD.OrderService.WebHost.Models
{
    public class CreateOrderRequest
    {
        ///<example>ca57f922-d1c4-4d97-a555-5aadc337223e</example>
        public Guid ClientId { get; set; }

        /// <example>
        /// [  { "dishId": "d47f7656-180e-4bda-bf3f-f63f1d1b839b",  "price": 123,  "discount": 4,  "count": 3 },
        ///    { "dishId": "e98a270a-22c1-49dd-b010-09fbbdcb69b7",  "price": 67.59,  "discount": 14,  "count": 2 },
        ///    { "dishId": "bbc58ea8-dbb4-431d-afe0-225f4ca8e118",  "price": 560.42,  "discount": 250,  "count": 4 },
        ///    { "dishId": "dd221c6b-3592-40b4-8e6c-dab0014fc1f9",  "price": 98.19,  "discount": 101,  "count": 21 }
        /// ]
        /// </example>
        public CreateOrderDetailRequest[] OrderDetails { get; set; } = [];
    }
}