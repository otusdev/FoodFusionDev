﻿using FFD.OrderService.WebHost.Consumers;
using MassTransit;

namespace WebApi
{
    public class ReceiveEndpointObserver : IReceiveEndpointObserver
    {
        private readonly ILogger<ReceiveEndpointObserver> _logger;
        public ReceiveEndpointObserver(ILogger<ReceiveEndpointObserver> logger) 
        {
            _logger = logger;
        }

        public Task Completed(ReceiveEndpointCompleted completed)
        {
            //throw new NotImplementedException();
            return Task.CompletedTask;
        }

        public Task Faulted(ReceiveEndpointFaulted faulted)
        {
            _logger.LogCritical(faulted.Exception.Message);
            return Task.CompletedTask;
            //throw faulted.Exception;
        }

        public Task Ready(ReceiveEndpointReady ready)
        {
            _logger.LogDebug($"MessageBroker: {(ready.ReceiveEndpoint as MassTransit.Transports.ReceiveEndpoint)?.Message}\r\n{ready.InputAddress.AbsoluteUri}");

            return Task.CompletedTask;
            //throw new NotImplementedException();
        }

        public Task Stopping(ReceiveEndpointStopping stopping)
        {
            //throw new NotImplementedException();
            return Task.CompletedTask;
        }
    }
}