﻿//using FFD.OrderService.Core.Entities;
//using FFD.OrderService.DataAccess.Repositories;
//using FFD.OrderService.WebHost.Models;
//using Microsoft.AspNetCore.Mvc;

//namespace FFD.OrderService.WebHost.Controllers
//{
//    [ApiController]
//    [Route("[controller]")]
//    public class ClientController : ControllerBase
//    {
//        readonly IRepository<Client> _clientRepository;

//        public ClientController(IRepository<Client> repository)
//        {
//            _clientRepository = repository;
//        }

//        /// <summary>
//        /// Создание нового клиента
//        /// </summary>
//        /// <param name="request"></param>
//        /// <returns></returns>
//        [HttpPost]
//        public async Task<ActionResult> CreateClientAsync(CreateOrEditClientRequest request)
//        {
//            var client = new Client()
//            {
//                Name = request.Name,
//                Email = request.Email,
//                PhoneNumber = request.PhoneNumber,
//            };

//            await _clientRepository.AddAsync(client);

//            return Ok(client.Id);
//        }

//        /// <summary>
//        /// Редактирование клиента
//        /// </summary>
//        /// <param name="id"></param>
//        /// <param name="request"></param>
//        /// <returns></returns>
//        [HttpPut("{id:guid}")]
//        public async Task<IActionResult> EditClientAsync(Guid id, CreateOrEditClientRequest request)
//        {
//            var client = await _clientRepository.GetByIdAsync(id);

//            if (client == null)
//                return NotFound();


//            client.Email = request.Email;
//            client.Name = request.Name;
//            client.PhoneNumber = request.PhoneNumber;


//            await _clientRepository.SaveChangesAsync();// .UpdateAsync(client);

//            return NoContent();
//        }

//        /// <summary>
//        /// Получение списка всех клиентов
//        /// </summary>
//        /// <returns>список клиентов</returns>
//        [HttpGet]
//        public async Task<IEnumerable<Client>> GetAllClientsAsync()//ActionResult Task<List<Table>>
//        {
//            return await _clientRepository.GetAllAsync();
//        }

//        /// <summary>
//        /// Получить клиента по id
//        /// </summary>
//        /// <param name="id">идентификатор заказа</param>
//        /// <returns></returns>
//        [HttpGet("{id:guid}")]
//        public async Task<ActionResult<Client>> GetClientsByIdAsync(Guid id)
//        {
//            var client = await _clientRepository.GetByIdAsync(id);//o => o.Id == id, ["Client"]

//            if (client == null)
//                return NotFound();

//            return client;
//        }

//        /// <summary>
//        /// Получить клиента по E-mail
//        /// </summary>
//        /// <param name="email">почта клиента</param>
//        /// <returns></returns>
//        [HttpGet("{email}")]
//        public async Task<ActionResult<Client>> GetClientByIdAsync(string email)
//        {
//            var client = (await _clientRepository.FindAllByAsync(c => c.Email == email))
//                                                                .FirstOrDefault();

//            if (client == null)
//                return NotFound();

//            return client;
//        }
        
//        /// <summary>
//        /// Удаление клиент
//        /// </summary>
//        /// <param name="id"></param>
//        /// <returns></returns>
//        [HttpDelete("{id:guid}")]
//        public async Task<IActionResult> DeleteClientAsync(Guid id)
//        {
//            int recAff = await _clientRepository.DeleteByIdAsync(id);

//            if (recAff == 0)
//                return NotFound();

//            return Ok($"Client id={id} has been deleted");
//        }
//    }
//}