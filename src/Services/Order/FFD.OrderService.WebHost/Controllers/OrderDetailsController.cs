﻿using FDD.Shared.Interfases;
using FFD.OrderService.Core.Entities;
using FFD.OrderService.DataAccess;
using FFD.OrderService.DataAccess.Repositories;
using FFD.OrderService.WebHost.Logic;
using FFD.OrderService.WebHost.Models;
using FFD.OrderService.WebHost.Notifier;
using FFD.Shared.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Net;

namespace FFD.OrderService.WebHost.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OrderDetailsController : ControllerBase
    {
        readonly IUnitOfWork _unitOfWork;

        readonly IRepository<OrderDetail> _orderDetailRepo;
        readonly IRepository<Order> _orderRepository;

        readonly OrderNotifier _notifier;
        readonly IUserAccessor _userAccessor;

        public OrderDetailsController(IUnitOfWork unitOfWork, 
                                      OrderNotifier notifier,
                                      IUserAccessor userAccessor)
        {            
            _unitOfWork = unitOfWork;

            //_orderRepository = _unitOfWork.GetRepository<Order>();
            _orderDetailRepo = _unitOfWork.GetRepository<OrderDetail>();

            _notifier = notifier;
            _userAccessor = userAccessor;
        }

        /// <summary>
        /// Редактирование позиции заказа с последующей отправкой в брокер сообщений
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut] //, Authorize
        public async Task<ActionResult> EditOrderDetailAsync(EditOrderDetailRequest request)
        {
            if (!Enum.IsDefined(typeof(OrderDetailStateEnum), request.State))// !Enum.TryParse<OrderStateType>(request.State.ToString(), out var state)
                return BadRequest($"Unknown orderDetail state: {request.State}");

            //get order position
            var orderPos = await _orderDetailRepo.GetByIdAsync(request.OrderId, request.DishId);
            if(orderPos == null) 
                return NotFound();

            //changed?            
            var newState = (OrderDetailStateEnum)request.State;
            if(newState == orderPos.State)
                return new StatusCodeResult((int)HttpStatusCode.NotModified);

            using (var statusTxn = await _unitOfWork.Context.Database.BeginTransactionAsync(IsolationLevel.Serializable))
            {
                //update orderDetails status
                orderPos.State = newState;
                orderPos.LastStateDate = DateTime.Now.ToUniversalTime();
                await _orderDetailRepo.SaveChangesAsync();

                //update order status
                if (newState == OrderDetailStateEnum.Ready || newState == OrderDetailStateEnum.Canceled)
                {
                    var cookingPositionCount = _orderDetailRepo.FindAllBy(p => p.OrderId == request.OrderId &&
                                                                    (p.State != OrderDetailStateEnum.Ready &&
                                                                     p.State != OrderDetailStateEnum.Canceled))
                                                                .Count();
                    //All positions are ready?
                    if (cookingPositionCount == 0)
                    {
                        OrderHelper orderHelper = new OrderHelper(_unitOfWork);
                        try
                        {
                            //set order status = ready
                            var order = await orderHelper.ChangeOrderStateAsync(request.OrderId, OrderStateEnum.Ready);
                            await orderHelper.AddOrderHistoryAsync(order);
                            statusTxn.Commit();
                        }
                        catch (Exception ex)
                        {
                            return BadRequest(ex.Message);
                        }
                    }
                }
               
            }

            //send to broker
            _notifier.NotifyMessage(orderPos);            

            return Ok(orderPos);
        }

        /// <summary>
        /// Добавление нового продукта в заказ
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        //[HttpPost]
        //public async Task<ActionResult> AddDishToOrderAsync(AddOrderDetailRequest request)
        //{
        //    //get order
        //    var order = await _orderRepository.GetByIdAsync(request.OrderId);

        //    if (order == null)
        //        return NotFound($"Order id={request.OrderId} not found");


        //using (var odTransaction = await _unitOfWork.Context.Database.BeginTransactionAsync(IsolationLevel.Serializable))
        //{
        //    double total = (await _orderDetailRepo.FindAllByAsync(d => d.OrderId == request.OrderId)).Sum(d => d.Total);

        //    //find position in order if exist
        //    var orderDetail = (await _orderDetailRepo.FindAllByAsync(d => d.OrderId == request.OrderId &&
        //                                                                  d.DishId == request.DishId))
        //                                             .FirstOrDefault();

        //    if (orderDetail == null)
        //        orderDetail = new OrderDetail();
        //    else total -= orderDetail.Total;

        //    orderDetail.Count = request.Count;
        //    orderDetail.Discount = request.Discount;
        //    orderDetail.Price = request.Price;

        //    if (orderDetail.OrderId == Guid.Empty)
        //    {
        //        orderDetail.OrderId = request.OrderId;
        //        orderDetail.DishId = request.DishId;
        //        await _orderDetailRepo.AddAsync(orderDetail); //saveChanges
        //    }
        //    else
        //        await _orderDetailRepo.SaveChangesAsync();

        //    //update Orders.total
        //    order.Total = total + orderDetail.Total;
        //    await _orderRepository.SaveChangesAsync();

        //    //commit
        //    await odTransaction.CommitAsync();
        //}

        //    return Ok(order);// orderDetail.Id);
        //}

        /// <summary>
        /// Удалить продукт из заказа
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        //[HttpDelete]
        //public async Task<ActionResult> DeleteProductDishFromOrderAsync(DeleteOrderDetailRequest request)
        //{
        //    //get order
        //    var order = await _orderRepository.GetByIdAsync(request.OrderId);

        //    if (order == null)
        //        return NotFound($"Order id={request.OrderId} not found");

        //    using (var odTransaction = await _unitOfWork.Context.Database.BeginTransactionAsync(IsolationLevel.Serializable))
        //    {
        //        //find position in order
        //        var orderDetail = (await _orderDetailRepo.FindAllByAsync(d => d.OrderId == request.OrderId &&
        //                                                                      d.DishId == request.DishId))
        //                                                 .FirstOrDefault();

        //        if (orderDetail == null)
        //            return NotFound($"Order id={request.OrderId} doesn't contain dishId={request.DishId}");

        //        //update Orders.total
        //        order.Total = (await _orderDetailRepo.FindAllByAsync(d => d.OrderId == request.OrderId)).Sum(d => d.Total) - orderDetail.Total;
        //        int recAff = await _orderRepository.SaveChangesAsync();

        //        //delete position
        //        recAff = await _orderDetailRepo.DeleteAsync(orderDetail); //saveChanges                  

        //        //commit
        //        await odTransaction.CommitAsync();
        //    }

        //    return Ok(order);// orderDetail.Id);
        //}

    }
}