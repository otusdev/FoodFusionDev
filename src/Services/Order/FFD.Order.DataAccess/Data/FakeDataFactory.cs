﻿using FFD.OrderService.Core.Entities;

namespace FFD.OrderService.DataAccess.Data
{
    public class FakeDataFactory
    {
        public static IEnumerable<Order> Orders => new List<Order>()
        {
            new ()
            {
                Id = Guid.Parse("6407b3fc-b672-4fa5-ba31-cf86ec07bc18"),
                Number = "1",
                Date = DateTime.Parse("2024-03-20 16:00:01"),
                State = OrderStateEnum.Delivered,
                StateDate = DateTime.Parse("2024-03-20 20:34:17"),
                Total = OrderDetails.Where(o=>o.OrderId == Guid.Parse("6407b3fc-b672-4fa5-ba31-cf86ec07bc18")).Sum(o=>o.Price*o.Count),// 115,
                ClientId = Guid.Parse("75a6fd5d-83bc-4658-8b78-9e280d8327f8"),
            },
            new ()
            {
                Id = Guid.Parse("44328385-ddbf-4e78-8b51-c9739e67078d"),
                Number = "2",
                Date = DateTime.Parse("2024-03-19 23:56:32"),
                State = OrderStateEnum.Open,
                StateDate = DateTime.Parse("2024-03-20 00:25:05"),
                Total = OrderDetails.Where(o=>o.OrderId == Guid.Parse("44328385-ddbf-4e78-8b51-c9739e67078d")).Sum(o=>o.Price*o.Count),//65,
                ClientId = Guid.Parse("ca57f922-d1c4-4d97-a555-5aadc337223e"),
            },
        };

        

        public static IEnumerable<OrderDetail> OrderDetails
        {  
            get
            {
                Guid orderId1 = Guid.Parse("6407b3fc-b672-4fa5-ba31-cf86ec07bc18");
                Guid orderId2 = Guid.Parse("44328385-ddbf-4e78-8b51-c9739e67078d");

                return new List<OrderDetail>()
                {
                    new()
                    {
                        OrderId = orderId1,
                        DishId = Guid.Parse("e98a270a-22c1-49dd-b010-09fbbdcb69b7"), //Трюфель
                        Count = 1,
                        Price = 150,
                        State = OrderDetailStateEnum.Ready,
                        LastStateDate =  DateTime.Parse("2024-03-20 18:15:38")
                    },
                    new()
                    {
                        OrderId = orderId1,
                        DishId = Guid.Parse("d47f7656-180e-4bda-bf3f-f63f1d1b839b"), //Тирамису
                        Count = 3,
                        Price = 400,
                        State = OrderDetailStateEnum.Ready,
                        LastStateDate =  DateTime.Parse("2024-03-20 18:16:39")
                    },

                    new()
                    {
                        OrderId = orderId2,
                        DishId = Guid.Parse("dd221c6b-3592-40b4-8e6c-dab0014fc1f9"), //Баскский чизкейк
                        Count = 4,
                        Price = 440,
                        State = OrderDetailStateEnum.Ready,
                        LastStateDate = DateTime.Parse("2024-03-20 08:25:05")
                    },
                    new()
                    {
                        OrderId = orderId2,
                        DishId = Guid.Parse("bbc58ea8-dbb4-431d-afe0-225f4ca8e118"), //Баскский чизкейк Имбирный пряник
                        Count = 1,
                        Price = 520,
                        State = OrderDetailStateEnum.Ready,
                        LastStateDate = DateTime.Parse("2024-03-20 08:26:26")

                    },
                    new()
                    {
                        OrderId = orderId2,
                        DishId = Guid.Parse("8b1b297b-0b47-4bca-9434-6052638c276b"), //Джелато йогурт страчателла
                        Count = 1,
                        Price = 600,
                        State = OrderDetailStateEnum.Ready,
                        LastStateDate = DateTime.Parse("2024-03-20 08:27:27")

                    },
                    new()
                    {
                        OrderId = orderId2,
                        DishId = Guid.Parse("67dc8108-427a-4a86-bbfd-22f0a2b1045c"), //Черничный торт
                        Count = 1,
                        Price = 400,
                        State = OrderDetailStateEnum.Cooking,
                        LastStateDate = DateTime.Parse("2024-03-20 08:28:08")


                    }
                };
            }
        }

        public static IEnumerable<OrderHistory> OrderHistories
        {
            get
            {
                Guid orderId1 = Guid.Parse("6407b3fc-b672-4fa5-ba31-cf86ec07bc18");
                Guid orderId2 = Guid.Parse("44328385-ddbf-4e78-8b51-c9739e67078d");

                return new List<OrderHistory>()
                {
                    new()
                    {
                        OrderId = orderId1,
                        State = OrderStateEnum.Open,
                        StateDate = DateTime.Parse("2024-03-20 16:00:01")                        
                        
                    },
                    new()
                    {
                        OrderId = orderId1,
                        State = OrderStateEnum.Paid,
                        StateDate = DateTime.Parse("2024-03-20 16:10:54")
                    },
                    new()
                    {
                        OrderId = orderId1,
                        State = OrderStateEnum.Delivered,
                        StateDate = DateTime.Parse("2024-03-20 20:34:17")
                    },
                    new()
                    {
                        OrderId = orderId2,
                        State = OrderStateEnum.Open,
                        StateDate = DateTime.Parse("2024-03-20 00:25:05")
                    }                    
                };
            }
        }


        //public static IEnumerable<Waiter> Waiters => new List<Waiter>()
        //{
        //    new ()
        //    {
        //        Id = Guid.Parse("99cde974-f314-4ef5-a2e1-87c3e2644d87"),
        //        Name = "Степан",
        //        Surname ="Степанов"
        //    },
        //    new ()
        //    {
        //        Id = Guid.Parse("27559b47-c7b5-4c32-ade7-a524441f6154"),
        //        Name = "Прасковья",
        //        Surname="Тулупова"
        //    }
        //};

        //public static IEnumerable<Table> Tables => new List<Table>()
        //{
        //    new ()
        //    {
        //        Id = Guid.Parse("2084effa-f911-47e4-97ce-dd5e4833f410"),
        //        Number = "1",
        //        SeatCount = 2,
        //        State = TableStateType.vacant
        //    },
        //    new ()
        //    {
        //        Id = Guid.Parse("39840a30-9aff-4275-bd5f-ff7b0af88c37"),
        //        Number = "2",
        //        SeatCount = 2,
        //        State = TableStateType.occupied
        //    },
        //    new ()
        //    {
        //        Id = Guid.Parse("595ff0a8-3f2f-4944-bf63-da338ff0523b"),
        //        Number = "3",
        //        SeatCount = 10,
        //        State = TableStateType.reserved
        //    },
        //    new ()
        //    {
        //        Id = Guid.Parse("a7cf5eeb-07c2-49e1-b42c-4c0d56cfd2a8"),
        //        Number = "4",
        //        SeatCount = 10,
        //        State = TableStateType.vacant
        //    }

        //};

        public static IEnumerable<Client> Clients => new List<Client>()
        {
            new ()
            {
                Id = Guid.Parse("75a6fd5d-83bc-4658-8b78-9e280d8327f8"),
                PhoneNumber = "322-223-322",
                Email = "Anonymous@gmail.com",
                Name = "Anonymous",
            },
            new ()
            {
                Id = Guid.Parse("ca57f922-d1c4-4d97-a555-5aadc337223e"),
                PhoneNumber = "007",
                Email = "James@mi6.uk",
                Name="James"
            },
            new ()
            {
                Id = Guid.Parse("b7cae62e-dbcd-43db-a59c-8b349262b56e"),
                PhoneNumber = "911",
                Email = "911@911.911",
                Name = "EmergencyService"
            }
        };

        public static IEnumerable<Dish> Dishes => new List<Dish>()
        {
            new ()
            {
                Id = Guid.Parse("e98a270a-22c1-49dd-b010-09fbbdcb69b7"),
                Name = "Трюфель",
                Price = 150,
                Weight = 20,
                Description ="Молочный (сливки, миндаль, крошка вафельная, шоколад молочный, какао масло), солёная карамель (сливки, масло сливочное, крошка вафельная, сахар, ваниль, глюкоза, шоколад молочный, соль), фисташка (сливки, глюкоза, паста фисташковая, шоколад белый, крошка вафельная, масло сливочное)"
            },
            new ()
            {
                Id = Guid.Parse("d47f7656-180e-4bda-bf3f-f63f1d1b839b"),
                Name = "Тирамису",
                Price = 440,
                Weight = 160,
                Description ="Сливки, яйцо, сыр маскарпоне, какао, кофе, печенье савоярди, сахар, соль"
            },
            new ()
            {
                Id = Guid.Parse("dd221c6b-3592-40b4-8e6c-dab0014fc1f9"),
                Name = "Баскский чизкейк",
                Price = 440,
                Weight = 150,
                Description ="Сливки, сок лимонный, ваниль, сахар, сыр творожный, яйцо, крахмал кукурузный, соль"
            },
            new ()
            {
                Id = Guid.Parse("bbc58ea8-dbb4-431d-afe0-225f4ca8e118"),
                Name = "Баскский чизкейк Имбирный пряник",
                Price = 520,
                Weight = 200,
                Description ="Сливки, сок лимонный, ваниль, сахар, сыр творожный, яйцо, крахмал кукурузный, печенье, соус (сливки, солёная карамель, сахар, имбирь, специи), сахар, соль"
            },
            new ()
            {
                Id = Guid.Parse("67dc8108-427a-4a86-bbfd-22f0a2b1045c"),
                Name = "Черничный торт",
                Price = 400,
                Weight = 130,
                Description ="Масло сливочное, яйцо, мука, загуститель, мируар нейтральный, разрыхлитель, сливки, черника, рикотта, сахар, глюкоза, соль"
            },
            new ()
            {
                Id = Guid.Parse("8b1b297b-0b47-4bca-9434-6052638c276b"),
                Name = "Джелато йогурт страчателла",
                Price = 600,
                Weight = 300,
                Description ="Для любителей классики на максималках: традиционный итальянский рецепт мороженого на свежих сливках, с щедрой порцией шоколадной крошки и нежной йогуртовой кислинкой. Практика показала, что это первое нетающее мороженое в мире. Секрет прост: нет времени, чтобы таять или откладывать на потом, съедается сразу"
            }

            #region old
            //new ()
            //{
            //    Id = Guid.Parse("e98a270a-22c1-49dd-b010-09fbbdcb69b7"),
            //    Name = "Кофе",
            //    Price = 300,
            //    Description ="черный"
            //},
            //new ()
            //{
            //    Id = Guid.Parse("d47f7656-180e-4bda-bf3f-f63f1d1b839b"),
            //    Name = "Булочка",
            //    Price = 100,
            //    Description ="свежая"
            //},

            //new ()
            //{
            //    Id = Guid.Parse("dd221c6b-3592-40b4-8e6c-dab0014fc1f9"),
            //    Name = "Водка",
            //    Price = 200,
            //    Description ="вкусная"
            //},
            //new ()
            //{
            //    Id = Guid.Parse("bbc58ea8-dbb4-431d-afe0-225f4ca8e118"),
            //    Name = "Жирная селёдка",
            //    Price = 151.5,                
            //}
#endregion
        };
    }
}
