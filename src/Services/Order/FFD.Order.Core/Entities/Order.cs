﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFD.OrderService.Core.Entities
{
    public class Order : BaseEntity
    {
        public Guid ClientId { get; set; }
        public Client Client { get; private set; }

        public string Number { get; set; }

       // [Column(TypeName = "timestamp without time zone")]
        public DateTime Date { get; set; }


        //public int StateId { get; set; }
        public OrderStateEnum State { get; set; }

        public DateTime StateDate { get; set; }

        public double Total {  get; set; }



        public List<OrderDetail> OrderDetails { get; }

        public List<OrderHistory> OrderHistories { get; }
        // public Table Table { get; set; }

        //  public List<Waiter> Waiters { get; set; } = [];
        // public List<Client> Clients { get; set; } = [];
    }
}
