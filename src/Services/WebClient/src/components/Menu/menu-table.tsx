import { useState, useEffect, useCallback, useMemo } from 'react';
import { Button, Form, Input, Modal, Popconfirm, Table, TableColumnsType } from 'antd';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import axios from 'axios';
import React from 'react';

const MenuTable = () => {

  const [data, setData] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState<{
    id: number;
    name: string;
  } | null>(null);
  const [selectedItem, setSelectedItem] = useState<DishDataType>();
  const [form] = Form.useForm();

  const showModal4Create = () => {
    setSelectedItem(null);
    form.resetFields();
    setIsModalVisible({ id: 0, name: "" });
  };

  const onCloseModal = () => {
    form.resetFields();
    setIsModalVisible(null);
  };

  const onClickSave = (dish: DishDataType) => {
    axios.post('/api/menubasket/Menu', dish)
      .then((response) => {
        fetchData()
        console.log(response.data)
      }).catch((e) => console.error(e))
  };

  const onClickEdit = (id : number ,dish: DishDataType) => {
    axios.put(`/api/menubasket/Menu/${id}`, dish)
      .then((response) => {
        fetchData()
        console.log(response.data)
      }).catch((e) => console.error(e))
  };

  const onEditGroup = useCallback(
    (record: DishDataType) => {
      setSelectedItem(record);
      setIsModalVisible({ id: record.id, name: record.name });
    }, [isModalVisible]
  );

  const handleDelete = (id: number) => {
    axios.delete(`/api/menubasket/Menu/${id}`)
    .then((response) => {
      fetchData()
      console.log(response.data)
    }).catch((e) => console.error(e))

  };

  const fetchData = async () => {
    try {
      const response = await axios.get('/api/menubasket/Menu', {
        headers: {
          Accept: 'application/json',
        },
      });
      setData(response.data);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    console.log("Монтируем MenuTable")
    fetchData(); // Вызываем функцию загрузки данных при монтировании компонента
  }, []);

  // const columns: TableColumnsType<DishDataType> = [
  //   {
  //     title: 'Id',
  //     dataIndex: 'id',
  //   },
  //   {
  //     title: 'Name',
  //     dataIndex: 'name',
  //   },
  //   {
  //     title: 'Price',
  //     dataIndex: 'price',
  //     sorter: {
  //       compare: (a, b) => a.price - b.price,
  //       multiple: 3,
  //     },
  //   },
  //   {
  //     title: 'Description',
  //     dataIndex: 'description',
  //   },
  //   {
  //     title: 'Edit',
  //     dataIndex: 'edit',
  //     align: 'center' as const,
  //     key: 'uedit',
  //     render: (_: any, record: any) => (
  //       <div>
  //         <Button type="primary" icon={<EditOutlined />}
  //           onClick={() => onEditGroup(record)}>Edit</Button>
  //       </div>
  //     ),
  //   },
  //   {
  //     title: 'Delete',
  //     dataIndex: 'delete',
  //     align: 'center' as const,
  //     key: 'udelete',
  //     render: (_: any, record: any) => (
  //       <Popconfirm title="Sure to delete?">
  //         <Button type="primary" icon={<DeleteOutlined />} danger
  //         >Delete</Button>
  //       </Popconfirm>
  //     ),
  //   },


  // ];

  const columns = useMemo(
    () => [
      {
        title: 'Id',
        dataIndex: 'id',
      },
      {
        title: 'Name',
        dataIndex: 'name',
      },
      {
        title: 'Price',
        dataIndex: 'price',
        sorter: {
          compare: (a: { price: number; }, b: { price: number; }) => a.price - b.price,
          multiple: 3,
        },
      },
      {
        title: 'Description',
        dataIndex: 'description',
      },
      {
        title: 'Edit',
        dataIndex: 'edit',
        align: 'center' as const,
        key: 'uedit',
        render: (_: any, record: any) => (
          <div>
            <Button type="primary" icon={<EditOutlined />}
              onClick={() => onEditGroup(record)}>Edit</Button>
          </div>
        ),
      },
      {
        title: 'Delete',
        dataIndex: 'delete',
        align: 'center' as const,
        key: 'udelete',
        render: (_: any, record: DishDataType) => (
          <Popconfirm title="Sure to delete?" onConfirm={() => handleDelete(record.id)}>
            <Button type="primary" icon={<DeleteOutlined />} danger
            >Delete</Button>
          </Popconfirm>
        ),
      },
    ], [onEditGroup],
  );

  return (<div>
    <Button type="primary" onClick={showModal4Create}>Create</Button>
    <Modal
      title="Создание\редактирование блюда"
      open={!!isModalVisible}
      footer={null}
      onCancel={onCloseModal}
    >
      <Form
        form={form}
        initialValues={selectedItem}
        layout="vertical"
        onFinish={values => {
          if (selectedItem == null) {
            console.log("selectedItem is null")
            onClickSave(values)
            onCloseModal()
          }
          else {
            onClickEdit(selectedItem.id,values)
            onCloseModal()
          }

        }}
      >
        <Form.Item
          name="name"
          label="Имя"
          rules={[{ required: true, message: 'Необходимо, ввести имя блюда!' }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="price"
          label="Стоимость"
          rules={[{ required: true, message: 'Необходимо, ввести стоимость блюда!' }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="description"
          label="Описание"
        >
          <Input />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">
            Сохранить
          </Button>
        </Form.Item>
      </Form>
    </Modal>
    <Table dataSource={data} columns={columns} /></div>
  );
};

export default MenuTable;