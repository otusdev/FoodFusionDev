import React from 'react';
import { Card, Button } from 'antd';
const { Meta } = Card;
import { useDispatch, useSelector } from 'react-redux';
import { Card as AntdCard } from 'antd';
// import Meta from 'antd/es/card/Meta';
import { PlusOutlined } from '@ant-design/icons';
import { deleteItemsFromCart, deleteItemFromCart, addItemInCart } from '@/redux/cart/reducer';
import styled from 'styled-components';

interface DishCardProps {
  // id: number
  // name: string;
  // description: string;
  imageUrl: string;
  item: DishDataType
}

export default function MenuItem({ item,imageUrl }: DishCardProps) {
  // const {
  //   getItemQuantity,
  //   increaseCartQuantity,
  //   decreaseCartQuantity,
  //   removeFromCart,
  // } = useShoppingCart()
  //const quantity = getItemQuantity(id)
  const dispatch = useDispatch();
  const items : DishDataType[] = useSelector((state: any) => state.cart.itemsInCart);
  const isItemInCart = items.some(someItem => someItem.id == item.id)

  const counter = items.reduce((accumulator, currentItem) => {
    if (currentItem.id === item.id) {
      return accumulator + currentItem.quantity;
    }
    return accumulator;
  }, 0);

  const handleAddClick = () => {
    //Тут сейчас сделано через редакс, но надо будет перекинуть на бэк
    console.log("+ блюдо в корзине")
    dispatch(addItemInCart(item))
  }
  const handleRemoveClick = () => {
    dispatch(deleteItemsFromCart([item.id]))
  }
  const deleteItem = () => {
    dispatch(deleteItemFromCart(item.id))
  }
  return (
    <Card
      style={{ width: 300, marginBottom: 20 }}
      cover={
        <img
          alt="example"         
          src={imageUrl}
          style={{ height: 200, objectFit: 'cover' }}
          referrerPolicy="no-referrer"
        />
      }
    >
      <Meta
        title={<h4>{item.name}</h4>}
        description={item.description}  
      />
      <p style={{ marginTop: 10, marginBottom: 5 }}>Цена:  {item.price} руб</p>
      <div style={{ textAlign: 'center', marginBottom: 10 }}>
        {isItemInCart === false ? (
          <Button type="primary" onClick={handleAddClick}>
            В корзину
          </Button>
        ) : (
          <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            <div>
              <Button onClick={handleAddClick}>+</Button>
              <span style={{ margin: '0 10px', fontSize: 18 }}>{counter}</span>
              <Button onClick={deleteItem}>-</Button>
            </div>
            <Button onClick={handleRemoveClick} style={{ marginLeft: 10 }} danger>
              Удалить
            </Button>
          </div>
        )}
      </div>
    </Card>
  )
}
