import { Button } from "antd"
import React, { useState } from "react"
import './cart-menu.css'
import { calcTotalPrice } from "./utils"
import { CartItem } from "../cart-item/cart-item"
import axios from "axios"
import { clearItemsFromCart } from "@/redux/cart/reducer"
import { useDispatch } from "react-redux"

type CartMenuProps = {
    items: DishDataType[]
}

export const CartMenu: React.FC<CartMenuProps> = ({ items }) => {
    const [currentOrderId, setCurrentOrderId] = useState<number>();
    const dispatch = useDispatch();
    function handleClick() {
        //Создание заказа на бэке 
        // var a = items.map(item => ({ "dishId": item.id, "dishQuantity": item.quantity }));
        // axios.post('/api/menubasket/Baskets', a)
        //     .then((response) => { console.log(response.data) }).catch((e) => console.error(e))

        //Получение новосозданного заказа
        // axios.get('/api/menubasket/Baskets')
        //     .then((response) => {
        //         const data = response.data
        //         const lastElement = data.length > 0 ? data[data.length - 1] : null;
        //         setCurrentOrderId(lastElement.id)
        //     }).catch((e) => console.error(e))

        // const sendToOrder = {
        //     basketId: currentOrderId,
        //     ListDish: items.map(item => ({

        //         "dishId": item.id,
        //         "dishName": item.name,
        //         "dishPrice": item.price,
        //         "dishDescpription": item.description,
        //         "dishQuantity": item.quantity,
        //     })),
        //     sum: calcTotalPrice(items),
        // };

        var sendToOrder = 
            items.map(item => ({
                "dishId": item.id,
                "dishName": item.name,
                "dishPrice": item.price,
                "dishDescpription": item.description,
                "dishQuantity": item.quantity,
            }))
        ;
        console.log(sendToOrder);

        axios.post('/api/menubasket/Baskets/SendToOrder', sendToOrder)
            .then((response) => { 
                console.log(response.data) 
                dispatch(clearItemsFromCart())
            }).catch((e) =>{ 
                console.error(e);
            })

    };
    // axios.post('/api/menubasket/Basket/SendToOrder', sendToOrder)
    //     .then((response) => { console.log(response.data) }).catch((e) => console.error(e))
    return (
        <div className="cart-menu">
            <div className="cart-menu__items-list"> {
                items.length > 0 ? items.map(item => (<CartItem title={item.name} price={item.price} count={item.quantity}></CartItem>)) : "Корзина пуста"
            }
            </div>
            {
                items.length > 0 ? (
                    <div className="cart-menu__arrange">
                        <div className="cart-menu__total-price">
                            <span>Итого:  </span>
                            <span>{calcTotalPrice(items)} руб</span>
                        </div>
                        <Button onClick={handleClick} >Сделать заказ</Button>
                    </div>) : null
            }
        </div>
    )
}


