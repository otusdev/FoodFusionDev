import { createAsyncThunk } from '@reduxjs/toolkit';
import { AxiosError } from 'axios';
// import { ILoginResponse, ThunkConfig } from '../../../../interfaces/common';
// import { USER_LOCALSTORAGE_REFRESH_TOKEN, USER_LOCALSTORAGE_TOKEN } from '../../../../constants/constants';
// import { userActions } from '../../../../features/userSlice';
import { ILoginResponse, ThunkConfig } from '@/interfaces/common';
import { USER_LOCALSTORAGE_REFRESH_TOKEN, USER_LOCALSTORAGE_TOKEN } from '@/constants';
import { userActions } from './userSlice';

interface LoginByUsernameProps {
    login: string;
    password: string;
    refreshTokenService?: boolean;
    isInternalUser?: boolean;
}

export const loginByUsername = createAsyncThunk<
    ILoginResponse,
    LoginByUsernameProps,
    ThunkConfig<AxiosError>
>(
    'login/loginByUsername',
    async (authData, thunkApi) => {
        const {
            extra, dispatch, rejectWithValue
        } = thunkApi;
        const { refreshTokenService, login, password, isInternalUser } = authData;
        // const isInternalUser = getState().loginForm?.isInternalUser;
        try {
            const response = await
            extra.api.post<ILoginResponse>(`/Auth/${!refreshTokenService
                ? 'Login' : 'RefreshToken'}`, !refreshTokenService
                ? { login, password, isInternalUser }
                : { login, refreshToken: localStorage.getItem(USER_LOCALSTORAGE_REFRESH_TOKEN) });

            if (!response.data) {
                throw new Error();
            }
            
            localStorage.setItem(USER_LOCALSTORAGE_TOKEN, response.data.body.accessToken);
            localStorage.setItem(USER_LOCALSTORAGE_REFRESH_TOKEN, response.data.body.refreshToken);
            if (response.data.body.requiredChangePassword) {
                dispatch(userActions.setUserInfo({ requiredChangePassword: true, userId: response.data.body.userId }));
            } else {
                dispatch(userActions.setUserInfo({ requiredChangePassword: false, userId: response.data.body.userId }));
                dispatch(userActions.login());
            }
            return response.data;
        } catch (e) {
            const typedError = e as AxiosError;
            return rejectWithValue(typedError);
        }
    },
);
