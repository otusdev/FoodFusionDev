import * as React from 'react';
import { useCallback, useEffect, useMemo, useState } from "react";
import { useAppDispatch } from "../../app/hooks";
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { Button, Form, GetProp, Input, Modal, Popconfirm, Select, Switch, Table, TablePaginationConfig, TableProps } from 'antd';
import { Avatar } from 'rsuite';
import { styled } from 'styled-components';
import Anonim from '../../images/Anonim.png';
import { useApi } from '../../services/caller';
import { getRouteCreateUser, getRouteDeleteUser, getRouteGetUserList, getRouteUpdateUser } from '@/constant/router';
import { AvatarSelector } from '@/components/AvatarSelector';
const { Option } = Select;
const TableWrapper = styled.div`
    display: flex;
    flex-direction: column;
    gap: 15px;
    .ant-table-cell {
        word-break: break-all;
    }
`;


const UserList = () => {
  const dispatch = useAppDispatch();
  const [isModalVisible, setIsModalVisible] = useState<{
    id: string;
    name: string;
  } | null>(null);
  const [searchString, setSearchString] = useState<string>('');
  const [pageSize, setPageSize] = useState<number>(5);
  const [page, setPage] = useState<number>(1);
  const [sorter, setSorter] = useState<any>();
  const [totalRecords, setTotalRecords] = useState<number>(0);
  const [userList, setUserList] = useState<any>([]);
  const [avatar, setAvatar] = React.useState<string>();
  const [selectedItem, setSelectedItem] = useState<any>();
  const isNewRecord = selectedItem?.id == null;
  const { data: userData, error: userError, success: userSuccess, loading: userLoading, fetchData: userFetchData } = useApi<any | any[]>(getRouteGetUserList, 'POST');
  const { data: recordData, error: recordError, success: recordSuccess, loading: recordLoading, fetchData: recordFetchData } = useApi<any | any[]>(isNewRecord ? getRouteCreateUser : getRouteUpdateUser, 'POST');
  const { data: deleteData, error: deleteError, success: deleteSuccess, loading: deleteLoading, fetchData: deleteFetchData } = useApi<any | any[]>(getRouteDeleteUser, 'DELETE');
  const [form] = Form.useForm();

  const handleTableChange: TableProps['onChange'] = (pagination, filters, sorter, extra) => {
    console.log('params', pagination, filters, sorter, extra);
    setPage(pagination.current);
    setPageSize(pagination.pageSize);
    setSorter(sorter);   

    // `dataSource` is useless since `pageSize` changed
    if (pagination.pageSize !== pageSize) {
      setUserList([])
    }
  };
 
  const onEditGroup = useCallback(
    (record: any) => {
      setSelectedItem(record);
      setIsModalVisible({ id: record.id, name: record.name });
    },
    [isModalVisible],
  );
  const columns =
    useMemo(
      () =>
        [
          {
            title: 'Id',
            dataIndex: 'id',
            key: 'id',
            align: 'center' as const,
          },
          {
            title: 'Avatar',
            dataIndex: 'avatar',
            key: 'avatar',
            align: 'center' as const,
            render: (_: any, record: any) => (
              <div>
                <Avatar
                  size="lg"
                  circle
                  src={record.avatar ? `${record.avatar}`  : Anonim }
                  style={{ marginLeft: 8 }} />
              </div>
            ),
          },
          {
            title: 'Login',
            dataIndex: 'login',
            sorter: true,
            key: 'login',
            align: 'left' as const,
          },
          {
            title: 'email',
            dataIndex: 'email',
            key: 'email',
            align: 'left' as const,
          },
          {
            title: 'Status',
            dataIndex: 'status',
            sorter: true,
            key: 'status',
            align: 'left' as const,
          },
          {
            title: 'loginFailures',
            dataIndex: 'loginFailures',
            sorter: true,
            key: 'loginFailures',
            align: 'left' as const,
          },
          {
            title: 'firstName',
            dataIndex: 'firstName',
            sorter: true,
            key: 'firstName',
            align: 'left' as const,
          },
          {
            title: 'lastName',
            dataIndex: 'lastName',
            sorter: true,
            key: 'lastName',
            align: 'left' as const,
          },
          {
            title: 'requiredChangePassword',
            dataIndex: 'requiredChangePassword',
            sorter: true,
            key: 'requiredChangePassword',
            align: 'left' as const,
          },
          {
            title: 'editGroupLabel',
            dataIndex: 'edit',
            align: 'center' as const,
            key: 'uedit',
            render: (_: any, record: any) => (
              <div>
                <Button type="primary" icon={<EditOutlined />}
                  onClick={() => onEditGroup(record)}>Edit</Button>
              </div>
            ),
          },
          {
            title: 'deleteGroupLabel',
            dataIndex: 'delete',
            align: 'center' as const,
            key: 'udelete',
            render: (_: any, record: any) => (
              <Popconfirm title="Sure to delete?" onConfirm={() => handleDelete(record)}>
                <Button type="primary" icon={<DeleteOutlined />} danger
                >Delete</Button>
              </Popconfirm>
            ),
          },
        ],
      [onEditGroup],
    );

  useEffect(() => {
  }, []);



  const fetchList = async () => {
    userFetchData({ page, pageSize, field: sorter?.field, order: sorter?.order })
  }

  useEffect(() => {
    if (userData?.total > 0) {
      setUserList(userData.list);
      setTotalRecords(userData.total);      
    }
  }, [userData]);

  const handleDelete = (record: any) => {
    deleteFetchData({ id: record.id });

  };

  useEffect(() => {
    fetchList();
  }, [page, pageSize, sorter,//JSON.stringify(tableParams), 
    recordData, deleteData])

  const onCloseModal = () => {
    setIsModalVisible(null);
  };

  const createRecord = () => {
    setSelectedItem(null);
    form.resetFields();
    setIsModalVisible({ id: "0", name: "" });
  }

  const onClickSave = () => {
    form
      .validateFields()
      .then((values) => {
        recordFetchData(values)
        form.resetFields();
      })
      .catch((info) => {
        console.log("Validate Failed:", info);
      });
  }
  useEffect(() => {
    if (recordData && recordSuccess) {
      setUserList(userData.list);
      onCloseModal();
    }
  }, [recordSuccess]);

  useEffect(() => {
    if (selectedItem) {
      if (selectedItem.avatar)
        setAvatar(selectedItem.avatar);
      else
      setAvatar(null);
      form.setFieldsValue({
        id: selectedItem.id,
        firstName: selectedItem.firstName,
        lastName: selectedItem.lastName,
        login: selectedItem.login,
        email: selectedItem.email,
        status: selectedItem.status,
        requiredChangePassword: selectedItem.requiredChangePassword,
        role: selectedItem.role,
        address: selectedItem.address,
      });
    }
  }, [selectedItem]);
  return (
    <div>
      {userLoading && <div>Loading...</div>}
      {!userSuccess && <div>Error: {JSON.stringify(userError)}</div>}
      <TableWrapper>
        <Button type="primary" onClick={createRecord}>Create</Button>
        <Table
          dataSource={userList}
          columns={columns}
          // pagination={false}f
          rowKey="id"
          loading={userLoading}
          // pagination={tableParams.pagination}
          pagination={{
            current: page,
            pageSize: pageSize,
            total: totalRecords,
            showSizeChanger: true,
            pageSizeOptions: [10, 20, 50, 100]
          }}
          onChange={handleTableChange}
        />
      </TableWrapper>
      <Modal
        title={isNewRecord ? 'Create new item' : 'Edit item'}
        open={!!isModalVisible}
        onOk={onClickSave}
        onCancel={onCloseModal}
        okText='Save'
        cancelText='Cancel'
      >
        <Form
          form={form}
          name="basic"
          initialValues={{ remember: true }}
          labelCol={{ span: 6 }}
          wrapperCol={{ span: 14 }}
        // layout="horizontal"        
        // style={{ maxWidth: 600 }}
        >
          <div>
            <Form.Item
              label="Id"
              name="id">
              <Input disabled />
            </Form.Item>
            <Form.Item
              label="Avatar"
              name="avatar"
              rules={[{ required: false }]}>
              <AvatarSelector
                onChange={setAvatar}
                userAvatar={avatar} />
            </Form.Item>
          </div>
          <Form.Item
            label="Фамилия"
            name="firstName"
            rules={[{ required: true, message: 'Please input your first name!' }]}              >
            <Input />
          </Form.Item>
          <Form.Item
            label="Имя"
            name="lastName"
            rules={[{ required: true, message: 'Please input your last name!' }]}>
            <Input />
          </Form.Item>
          <Form.Item
            label="Логин"
            name="login"
            rules={[{ required: true, message: 'Please input your login!' }]}>
            <Input />
          </Form.Item>
          <Form.Item
            label="Email"
            name="email"
            rules={[{ required: true, message: 'Please input your email!', type: 'email' }]}>
            <Input />
          </Form.Item>
          <Form.Item
            label="Пароль"
            name="password">
            <Input />
          </Form.Item>
          <Form.Item
            label="Статус"
            name="status"
            rules={[{ required: true, message: 'Please select status!' }]}>
            <Select>
              <Option value="Active">Active</Option>
              <Option value="Blocked">Blocked</Option>
              <Option value="Disabled">Disabled</Option>
            </Select>
          </Form.Item>
          <Form.Item
            label="Роль"
            name="role"
            rules={[{ required: true, message: 'Please select role!' }]}>
            <Select>
              <Option value="Client">Клиент</Option>
              <Option value="Admin">Админ</Option>
              <Option value="Kitchen">Повар</Option>
              <Option value="Courier">Курьер</Option>
            </Select>
          </Form.Item>
          <Form.Item
            label="Require Change Password"
            name="requiredChangePassword"
            valuePropName="checked"          >
            <Switch disabled />
          </Form.Item>
          <Form.Item
            label="Адрес"
            name="address">
            <Input />
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
};
export default UserList;





