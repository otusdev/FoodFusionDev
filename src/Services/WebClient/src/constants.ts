export const USER_LOCALSTORAGE_EMAIL = 'email';
export const USER_LOCALSTORAGE_TOKEN = 'token';
export const USER_LOCALSTORAGE_SIGNED = 'signed';
export const USER_LOCALSTORAGE_REFRESH_TOKEN = 'refresh';
export const THEME_LOCALSTORAGE_KEY = 'app-theme';
