﻿using System.ComponentModel;

public enum RoleEnum {
    [Description("Администратор")]
    Admin = 1,

    [Description("Повар")]
    Cook = 2,

    [Description("Курьер")]
    Courier = 3,

    [Description("Клиент")]
    Client = 4,

}