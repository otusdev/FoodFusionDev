﻿using System.ComponentModel;

public enum OrderStateEnum {
    [Description("Заказ отменен")]
    Canceled = 1,

    [Description("Заказ открыт")]
    Open = 2,

    [Description("Заказ оплачен")]
    Paid = 3,

    [Description("Заказ приготовлен")]
    Ready = 4,

    [Description("Заказ доставлен")]
    Delivered = 5
}