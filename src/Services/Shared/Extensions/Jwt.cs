﻿using FFD.Shared.Models.Settings;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFD.Shared.Extensions
{
    public class JwtService
    {
        public static JwtBearerOptions GetJwtBearerOptions(AuthOptions authOptions)
        {
            var jwtBearerOptions = new JwtBearerOptions
            {
                RequireHttpsMetadata = true,
                SaveToken = true,
                TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidIssuer = authOptions.JwtToken.Issuer,
                    ValidateAudience = true,
                    ValidAudience = authOptions.JwtToken.Audience,
                    ValidateLifetime = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(authOptions.JwtToken.SecretKey)),
                    ValidateIssuerSigningKey = true
                }
            };

            return jwtBearerOptions;
        }
    }
}
//    public static void AddJwt(this IServiceCollection services, IConfiguration configuration)
//    {
//        var authenticationSection = configuration.GetSection("Authentication").Value;
//        var authOptions = authenticationSection.Get<AuthOptions>();
//        builder.Services.Configure<AuthOptions>(authenticationSection);
//        var options = new JwtTokenOptions();
//        var section = configuration.GetSection("jwt");
//        section.Bind(options);
//        services.Configure<JwtOptions>(section);
//        services.AddSingleton<IJwtBuilder, JwtBuilder>();
//        services.AddAuthentication()
//            .AddJwtBearer(cfg =>
//            {
//                cfg.RequireHttpsMetadata = false;
//                cfg.SaveToken = true;
//                cfg.TokenValidationParameters = new TokenValidationParameters
//                {
//                    ValidateAudience = false,
//                    IssuerSigningKey = new SymmetricSecurityKey
//                                       (Encoding.UTF8.GetBytes(options.Secret))
//                };
//            });
//    }
//    public class AuthOptions
//    {
//        public JwtTokenOptions JwtToken { get; set; }
//    }

//    public class JwtTokenOptions
//    {
//        public string Issuer { get; set; } = "Tempalte"; // издатель токена
//        public string Audience { get; set; } = "Audience"; // потребитель токена
//        public string SecretKey { get; set; } = "super!secretkey@";   // ключ для шифрации
//        public int Lifetime { get; set; } = 600;
//    }
//}
//var authenticationSection = builder.Configuration.GetSection("Authentication");
//var authOptions = authenticationSection.Get<AuthOptions>();
//builder.Services.Configure<AuthOptions>(authenticationSection);
//builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
//    .AddJwtBearer(options =>
//    {
//        options.RequireHttpsMetadata = true;
//        options.SaveToken = true;
//        options.TokenValidationParameters = new TokenValidationParameters
//        {
//            ValidateIssuer = true,
//            ValidIssuer = authOptions.JwtToken.Issuer,
//            ValidateAudience = true,
//            ValidAudience = authOptions.JwtToken.Audience,
//            ValidateLifetime = true,
//            IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(authOptions.JwtToken.SecretKey)),
//            ValidateIssuerSigningKey = true
//        };
//    });
