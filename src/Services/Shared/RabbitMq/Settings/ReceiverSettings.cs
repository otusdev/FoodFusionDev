﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFD.Shared.RabbitMq.Settings
{
    public class ReceiverSettings
    {
        public string Queue { get; set; }
        public bool Durable { get; set; }
        public string Exchange { get; set; }
        public string ExchangeType { get; set; }
        public List<string> Keys { get; set; }
        public bool AutoAck { get; set; }
        public string RoutingKeys { get; set; }
         
    }
}
