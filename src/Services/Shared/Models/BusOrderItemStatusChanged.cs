﻿namespace FFD.Shared.Models
{
    public class BusOrderItemStatusChanged
    {
        public UserDto? User { get; set; }

        public Guid OrderId { get; set; }
        public Guid DishId { get; set; }
        public uint Count { get; set; }
        public int State { get; set; }
        public string StateText { get; set; }
    }
}