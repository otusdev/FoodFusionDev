﻿using System.Security.Claims;

namespace FDD.Shared.Interfases
{
    public interface IUserAccessor
    {
        ClaimsPrincipal User { get; }
        Guid? Id { get; }
        string? Role { get; }      
        string? FirstName { get; }
        string? LastName { get; }
        string? MiddleName { get; }
        string? Login { get; }
        string? Email { get; }
    }
}
