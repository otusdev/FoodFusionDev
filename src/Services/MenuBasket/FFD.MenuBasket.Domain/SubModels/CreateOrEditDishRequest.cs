﻿
namespace FFD.MenuBasket.Domain.SubModels
{
    public class CreateOrEditDishRequest
    {
        public string? Name { get; set; }
        public int? Price { get; set; }
        public string? Description { get; set; }
        public string? Category { get; set; }
    }
}
