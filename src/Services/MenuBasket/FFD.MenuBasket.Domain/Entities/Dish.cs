﻿
using System.Collections.Generic;

namespace FFD.MenuBasket.Domain.Entities
{
    public class Dish : BaseEntity
    {
        public string? Name { get; set; }
        public int? Price { get; set; }
        public string? Description { get; set; }
        public string? Category { get; set; }
        public IList<DishBasket>? DishBasket { get; set; }
        public override string ToString() => Name ?? "EmptyName";

    }
}
