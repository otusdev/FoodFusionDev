﻿

using System;
using System.Collections.Generic;

namespace FFD.MenuBasket.Domain.Entities
{
    public class Basket : BaseEntity
    {
        public string? CustomName { get; set; }
        public IList<DishBasket>? DishBasket { get; set; }
        public string BasketCreatedDate { get; set; }
        public Guid CustomerId { get; set; }

        public override string ToString()
        {
            return CustomName ?? "EmptyName";
        }
    }
}
