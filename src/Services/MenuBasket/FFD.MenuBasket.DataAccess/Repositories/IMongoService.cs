﻿using FFD.MenuBasket.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFD.MenuBasket.DataAccess.Repositories
{
    public interface IMongoService
    {
        public DishImage Create(DishImage dishImage);
        public List<DishImage> Get();
    }
}
