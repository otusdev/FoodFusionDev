﻿using FFD.MenuBasket.Domain.Entities;
using FFD.MenuBasket.Domain.SubModels;
using FFD.Shared.Models;

namespace FFD.MenuBasket.DataAccess.Repositories
{
    public interface IRepository<T> where T : BaseEntity
    {
        //Common
        Task<IEnumerable<T>> GetAllAsync();
        Task<T> GetByIdAsync(int id);
        //Dish
        Task<int> AddNewDishAsync(Dish dish);
        Task<bool> DeleteDishByIdAsync(int id);
        //Basket
        Task<int> PostNewBasketWithDishesAsync(Basket basket, params CreateBasketWithDishAndQuantities[]? req);
        Task<bool> DeleteBasketbyIdAsync(int id);
        Task<bool> AddDishes2BasketAsync(int id, int dishId);
        Task<bool> DeleteDishFromBasketAsync(int id, int dishId);
        Task<IEnumerable<Dish>> GetDishesByUserBasket(Guid userId);
        Task<IEnumerable<Basket>> GetBasketsByUser(Guid userId);
        Task<long> SaveOrder(List<ListDish> list);
    }
}
