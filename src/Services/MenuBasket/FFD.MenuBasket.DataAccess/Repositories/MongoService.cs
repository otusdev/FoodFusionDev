﻿using FFD.MenuBasket.Domain.Entities;
using MongoDB.Driver;

namespace FFD.MenuBasket.DataAccess.Repositories
{
    public class MongoService : IMongoService
    {
        private readonly IMongoCollection<DishImage> _dishImageRepository;
        public MongoService(IMongoDatabase database)
        {
            _dishImageRepository = database.GetCollection<DishImage>("dish_images");
        }

        public DishImage Create(DishImage dishImage)
        {
            _dishImageRepository.InsertOne(dishImage);
            return dishImage;
        }

        public List<DishImage> Get()
        {
            return _dishImageRepository.Find(di => true).ToList();
        }



    }
}
