﻿using FFD.MenuBasket.DataAccess.Data;
using FFD.MenuBasket.Domain.Entities;
using MongoDB.Driver;

namespace FFD.MenuBasket.DataAccess.Mongo
{
    public static class MongoDataBaseSeeder
    {
        public static void DbSeed(IMongoDatabase db)
        {
            var collection = db.GetCollection<DishImage>("dish_images");
            var filter = Builders<DishImage>.Filter.Empty;
            if (collection.CountDocuments(filter) == 0)
            {
                collection.InsertMany(FakeDataFactory.DishImages);
            }
        }
    }
}
