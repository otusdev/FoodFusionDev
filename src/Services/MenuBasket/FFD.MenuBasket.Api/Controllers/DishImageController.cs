﻿using FFD.MenuBasket.DataAccess.Repositories;
using FFD.MenuBasket.Domain.Entities;
using Microsoft.AspNetCore.Mvc;

namespace FFD.MenuBasket.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DishImageController : ControllerBase
    {
        private readonly IMongoService _mongoService;

        public DishImageController(IMongoService mongoService)
        {
            _mongoService = mongoService;
        }

        /// <summary>
        /// Create picture
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        //public ActionResult<DishImage> Post([FromBody] DishImage dishImage)
        public ActionResult<DishImage> Post(string name, string imageUrl)
        {
            var dishImage = new DishImage()
            {
                ShortName = name,
                ImageUrl = imageUrl
            };
            _mongoService.Create(dishImage);
            return CreatedAtAction(nameof(GetDishImages), new { id = dishImage.Id }, dishImage);
        }

        /// <summary>
        /// Получить данные всех dishImage
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<List<DishImage>> GetDishImages()
        {
            return _mongoService.Get();
        }

    }
}
