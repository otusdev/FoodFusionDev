﻿using Microsoft.EntityFrameworkCore;
using FFD.Auth.Core.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFD.Auth.Infrastructure.Extensions
{
     public static class QueryableExtensions
    {
        public static IQueryable<T> ApplySorting<T>(this IQueryable<T> query, string? field, string? order)
        {
            if (!string.IsNullOrEmpty(field) && !string.IsNullOrEmpty(order))
            {
                var sortField = typeof(T).GetProperties().FirstOrDefault(x => x.Name.Equals(field, StringComparison.InvariantCultureIgnoreCase));
                if (sortField != null)
                {
                    query = order.ToLower() == "descend"
                        ? query.OrderByDescending(x => EF.Property<object>(x, sortField.Name))
                        : query.OrderBy(x => EF.Property<object>(x, sortField.Name));
                }
            }
            return query;
        }

        public static IQueryable<T> ApplyPagination<T>(this IQueryable<T> query, int Current,  int PageSize )
        {
            return query.Skip((Current - 1) * PageSize).Take(PageSize);
        }
    }
}
