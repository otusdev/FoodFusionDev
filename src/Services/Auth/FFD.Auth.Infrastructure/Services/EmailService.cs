﻿using FFD.Auth.Core.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using FFD.Auth.Core.Settings;
using FFD.Auth.Core;
using Microsoft.Extensions.Options;
using System.Security.Cryptography;
using MimeKit;
using MailKit.Security;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.EntityFrameworkCore;

namespace FFD.Auth.Infrastructure.Services
{
    public class EmailService : IEmailService
    {
        private readonly EmailSettings _emailSettings;
        private readonly IDbContext _dbContext;

        public EmailService(IDbContext dbContext, IOptions<EmailSettings> emailSettings)
        {
            _dbContext = dbContext;
            _emailSettings = emailSettings.Value;
        }

        public async Task SendEmailAsync(string toEmail, string subject, string message)
        {
            try
            {
                var email = new MimeMessage();
                email.From.Add(new MailboxAddress(_emailSettings.FromName, _emailSettings.FromEmail));
                email.To.Add(MailboxAddress.Parse(toEmail));
                email.Subject = subject;

                var builder = new BodyBuilder
                {
                    HtmlBody = message
                };

                email.Body = builder.ToMessageBody();

                using var smtp = new SmtpClient();
                await smtp.ConnectAsync(_emailSettings.SmtpServer, _emailSettings.SmtpPort, SecureSocketOptions.StartTls);
                await smtp.AuthenticateAsync(_emailSettings.SmtpUsername, _emailSettings.SmtpPassword);
                await smtp.SendAsync(email);
                await smtp.DisconnectAsync(true);
            }
            catch (Exception ex)
            {
                // Handle the exception as needed, such as logging the error
                throw;
            }
        }
        public async Task SendConfirmationEmailAsync(Guid userId)
        {
            var user = await _dbContext.Users.FirstOrDefaultAsync(x=>x.Id == userId);
            if (user == null)
            {
                throw new InvalidOperationException("Пользователь не найден.");
            }
            // Генерация токена подтверждения почты
            var token = GenerateEmailConfirmationToken();
            user.EmailConfirmationToken = token;
            await _dbContext.SaveChangesAsync();

            // Формирование ссылки для подтверждения почты
            var confirmationLink = $"https://ffd.naufan.keenetic.pro/api/Auth/Confirm?email={user.Email}&code={token}";

            await SendEmailAsync(user.Email, "Подтверждение почты", $"Для подтверждения вашей почты перейдите по ссылке: <a href=\"{confirmationLink}\">Подтвердить</a>");

        }

        private string GenerateEmailConfirmationToken()
        {
            // Генерация случайного токена
            byte[] tokenBytes = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(tokenBytes);
            }
            return WebEncoders.Base64UrlEncode(tokenBytes);
        }
    }
}
