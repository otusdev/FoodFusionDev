﻿using FFD.Auth.Core.Entities.Db;
using FFD.Auth.Core.IServices;
using FFD.Auth.Core.Settings;
using FFD.Auth.Core.User;
using FFD.Shared.Models.Settings;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace FFD.Auth.Infrastructure.Services
{
    public class JwtTokenGenerator : ITokenGenerator
    {
        private readonly JwtTokenOptions _jwtTokenOptions;

        public JwtTokenGenerator(IOptions<AuthOptions> authOptions)
        {
            _jwtTokenOptions = authOptions.Value.JwtToken;
        }

        public string Generate(List<Claim> claims)
        {
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Expires = DateTime.UtcNow.AddMinutes(_jwtTokenOptions.Lifetime),
                Issuer = _jwtTokenOptions.Issuer,
                Audience = _jwtTokenOptions.Audience,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_jwtTokenOptions.SecretKey)),
                                                                SecurityAlgorithms.HmacSha512Signature),
                Subject = new ClaimsIdentity(claims)
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }
        public bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new HMACSHA512(passwordSalt))
            {
                var computeHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                return computeHash.SequenceEqual(passwordHash);
            }
        }
        public RefreshToken GenerateRefreshToken()
        {
            var refreshToken = new RefreshToken
            {
                Token = Convert.ToBase64String(RandomNumberGenerator.GetBytes(64)),
                Expires = DateTime.Now.AddDays(30),
                Created = DateTime.Now
            };

            return refreshToken;
        }
        public void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }
        public List<Claim> GenerateClaims(User user)
        {
            var userClaims = new List<Claim>
            {
                new Claim("Id", user.Id.ToString()),
                new Claim(ClaimTypes.NameIdentifier, user.Login),
                new Claim("Role", user.Role.ToString()),
                new Claim(ClaimTypes.Email, user.Email),
                new Claim("FirstName", user.FirstName),
                new Claim("LastName", user.LastName),              
                //new Claim("MiddleName", user.MiddleName),
                new Claim("Login", user.Login),
                new Claim("Email", user.Email),  
                //new Claim("TypeUser", user.Type.ToString()),
            };
            if (user.Address != null)
                userClaims.Add(new Claim("Address", user.Address));
            if (user.RequiredChangePassword != null)
                userClaims.Add(new Claim("RequiredChangePassword", user.RequiredChangePassword?.ToString()));
            if (!string.IsNullOrEmpty(user.Email))
                userClaims.Add(new Claim(ClaimTypes.Email, user.Email));
            //var roles = user.Group?.Roles;
            //if (roles != null)
            //    foreach (var role in roles)
            //    {
            //        userClaims.Add(new Claim(ClaimTypes.Role, role.Name));
            //    }
            return userClaims;
        }
    }
}
