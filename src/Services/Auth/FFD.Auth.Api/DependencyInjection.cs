﻿using FFD.Auth.Core.IServices;
using FFD.Auth.Core;
using FFD.Auth.Infrastructure.Services;
using FFD.Auth.Infrastructure;
using FDD.Shared.Interfases;
using FFD.Auth.Core.Settings;
using FDD.Shared.Services;

namespace FFD.Auth.Api
{
    public static class DependencyInjection
    {
        public static void InjectDependencies(this WebApplicationBuilder builder)
        {
            builder.Services.AddScoped<IDbContext, ApplicationDbContext>();

            builder.Services.AddScoped<ITokenGenerator, JwtTokenGenerator>();

            builder.Services.AddScoped<IUserAccessor, UserAccessor>();

            builder.Services.Configure<EmailSettings>(builder.Configuration.GetSection("EmailSettings"));

            builder.Services.AddScoped<IEmailService, EmailService>();
        }
    }
}
