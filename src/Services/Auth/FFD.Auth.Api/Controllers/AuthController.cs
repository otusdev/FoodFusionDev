﻿using FFD.Auth.Application.CQRS.Authentication.Dto;
using FFD.Auth.Application.CQRS.Authentication.Queries;
using FFD.Auth.Application.CQRS.User.Commands;
using FFD.Auth.Core;
using FFD.Auth.Core.User;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace FFD.Auth.Api.Controllers
{
    [ApiController]   
    public class AuthController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ILogger<UserController> _logger;
        public AuthController(IMediator mediator, ILogger<UserController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost("Login")]
        public async Task<Response<TokenDto>> LoginAsync(GetAccessTokenQuery request, CancellationToken cancellationToken = default)
        {
            try
            {
                var result = await _mediator.Send(request, cancellationToken);
                return Response<TokenDto>.Success(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                throw ex;
            }
        }
        [HttpPost("RefreshToken")]
        public async Task<Response<TokenDto>> RefreshToken(GetRefreshTokenQuery request, CancellationToken cancellationToken = default)
        {
            try
            {
                var result = await _mediator.Send(request, cancellationToken);

                return Response<TokenDto>.Success(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                throw ex;
            }
        }

        [HttpGet("user"), Authorize]
        public async Task<Response<UserInfoDto>> GetCurrentUserInfo(CancellationToken cancellationToken = default)
        {
            var result = await _mediator.Send(new GetCurrentUserInfoQuery(), cancellationToken);

            return Response<UserInfoDto>.Success(result);
        }

        [HttpGet("confirm")]
        public async Task<Response<bool>> ConfirmEmail(string email, string code, CancellationToken cancellationToken = default)
        {
            var result = await _mediator.Send(new ConfirmUserCommand() { Code = code, Email = email }, cancellationToken);

            return Response<bool>.Success(result);
        }
        

        [HttpGet("testApi")]
        public string testApi(string request, CancellationToken cancellationToken = default)
        {
            return request;
        }
        //[HttpPost("refreshToken")]
        //public async Task<Response<TokenDto>> RefreshToken(GetRefreshTokenQuery request, CancellationToken cancellationToken = default)
        //{
        //    var result = await _mediator.Send(request, cancellationToken);

        //    return Response<TokenDto>.Success(result);
        //}
        //[Authorize]
        //[HttpGet("user")]
        //public async Task<IActionResult> GetCurrentUserInfo(CancellationToken cancellationToken = default)
        //{
        //    var result = await _mediator.Send(new GetCurrentUserInfoQuery(), cancellationToken);

        //    return Ok(result);
        //}
        //[Authorize]
        //[HttpPost("ResetUserPassword")]
        //public async Task<IActionResult> ResetUserPassword(ResetUserPasswordCommand request, CancellationToken cancellationToken = default)
        //{
        //    var result = await _mediator.Send(request, cancellationToken);
        //    return Ok(result);
        //}

        //[HttpPost("ResetUserPasswordConfirm")]
        //public async Task<IActionResult> ResetUserPasswordConfirm(ResetUserPasswordConfirmCommand request, CancellationToken cancellationToken = default)
        //{
        //    var result = await _mediator.Send(request, cancellationToken);
        //    return Ok(result);
        //}
    }
}
