﻿using AutoMapper;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;
using Microsoft.EntityFrameworkCore;
using FFD.Auth.Core;
using FFD.Auth.Core.IServices;
using FFD.Auth.Application.Exceptions;

namespace FFD.Auth.Application.CQRS.User.Commands
{
    public class CreateUserCommand : IRequest<Guid>
    {
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? MiddleName { get; set; }        
        public string? Login { get; set; }
        public string? Email { get; set; }
        public string? Password { get; set; }
        public string? Avatar { get; set; }    
        public string? Status { get; set; }
        public string? Role { get; set; }
        public string? Address { get; set; }

    }

    public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, Guid>
    {
        private readonly IDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly ITokenGenerator _tokenGenerator;

        public CreateUserCommandHandler(IDbContext dbContext, IMapper mapper, ITokenGenerator tokenGenerator)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _tokenGenerator = tokenGenerator;
        }

        public async Task<Guid> Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {
            var user = _mapper.Map<FFD.Auth.Core.Entities.Db.User>(request);
            var userExist = await _dbContext.Users.AnyAsync(x => x.Email == request.Email);
            if (userExist)
            {
                throw new UserIsExistException();
            }
            _tokenGenerator.CreatePasswordHash(request.Password, out byte[] passwordHash, out byte[] passwordSalt);
            user.PasswordSalt = passwordSalt;
            user.PasswordHash = passwordHash;
            await _dbContext.Users.AddAsync(user);
            await _dbContext.SaveChangesAsync();
            return user.Id;
        }
    }
    public sealed class CreateUserCommandValidator : AbstractValidator<CreateUserCommand>
    {
        public CreateUserCommandValidator()
        {
            RuleFor(x => x.Email).NotEmpty();
        }
    }
}
