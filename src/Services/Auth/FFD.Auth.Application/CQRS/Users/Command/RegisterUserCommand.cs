﻿using AutoMapper;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;
using Microsoft.EntityFrameworkCore;
using FFD.Auth.Core;
using FFD.Auth.Core.IServices;
using FFD.Auth.Application.Exceptions;
using FFD.Auth.Infrastructure.Services;

namespace FFD.Auth.Application.CQRS.User.Commands
{
    public class RegisterUserCommand : IRequest<Guid>
    {
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? MiddleName { get; set; }        
        public string? Login { get; set; }
        public string? Email { get; set; }
        public string? Password { get; set; }
        public string? Address { get; set; }

    }

    public class RegisterUserCommandHandler : IRequestHandler<RegisterUserCommand, Guid>
    {
        private readonly IDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly ITokenGenerator _tokenGenerator;
        private readonly IEmailService _emailService;

        public RegisterUserCommandHandler(IDbContext dbContext, IMapper mapper, ITokenGenerator tokenGenerator, IEmailService emailService)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _tokenGenerator = tokenGenerator;
            _emailService = emailService;
        }

        public async Task<Guid> Handle(RegisterUserCommand request, CancellationToken cancellationToken)
        {
            var user = _mapper.Map<FFD.Auth.Core.Entities.Db.User>(request);
            var userExist = await _dbContext.Users.AnyAsync(x => x.Email == request.Email);
            if (userExist)
            {
                throw new UserIsExistException();
            }
            _tokenGenerator.CreatePasswordHash(request.Password, out byte[] passwordHash, out byte[] passwordSalt);
            user.PasswordSalt = passwordSalt;
            user.PasswordHash = passwordHash;
            await _dbContext.Users.AddAsync(user);
            await _dbContext.SaveChangesAsync();
            if (request.Email != null)
                try
                {
                    await _emailService.SendConfirmationEmailAsync(user.Id);
                }
                catch (Exception ex)
                {
                    throw new EmailException();
                }
            return user.Id;
        }
    }
    public sealed class RegisterUserCommandValidator : AbstractValidator<RegisterUserCommand>
    {
        public RegisterUserCommandValidator()
        {
            RuleFor(x => x.Email).NotEmpty();
        }
    }
}
