﻿using AutoMapper;
using FFD.Auth.Core;
using FFD.Auth.Core.Entities.Dto;
using FFD.Auth.Core.Query;
using FFD.Auth.Infrastructure.Extensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;


namespace FFD.Auth.Application.CQRS.User.Queries
{
    public class GetUsersQuery : IRequest<ResponsibleList<UserDto>>
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string? Order { get; set; }
        public string? Field { get; set; }
    }

    public class GetUsersQueryHandler : IRequestHandler<GetUsersQuery, ResponsibleList<UserDto>>
    {
        private readonly IMapper _mapper;
        private readonly IDbContext _dbContext;

        public GetUsersQueryHandler(IMapper mapper, IDbContext dbContext)
        {
            _mapper = mapper;
            _dbContext = dbContext;
        }

        public async Task<ResponsibleList<UserDto>> Handle(GetUsersQuery request, CancellationToken cancellationToken)
        {
            IQueryable<FFD.Auth.Core.Entities.Db.User>? query = _dbContext.Users.AsNoTracking();
            var total = query.Count();
            query = query.ApplySorting(request.Field, request.Order);
            query = query.ApplyPagination(request.Page, request.PageSize);

            //query = query.Skip((request.Pagination.Current - 1) * request.Pagination.PageSize).Take(request.Pagination.PageSize);
            var users = await query.ToListAsync(cancellationToken);
            IEnumerable<UserDto>? list = _mapper.Map<IEnumerable<UserDto>>(users);
            var result = new ResponsibleList<UserDto>()
            {
                Total = total,
                List = list
            };
            return result;
        }
    }
}
