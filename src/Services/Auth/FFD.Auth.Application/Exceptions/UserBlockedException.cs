﻿namespace FFD.Auth.Application.Exceptions
{
    public class UserBlockedException : ApplicationException
    {
        public UserBlockedException(string message = "User blocked") : base(message)
        {
        }
    }
}
