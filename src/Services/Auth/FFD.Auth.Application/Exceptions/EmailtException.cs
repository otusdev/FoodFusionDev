﻿namespace FFD.Auth.Application.Exceptions
{
    public class EmailException : ApplicationException
    {
        public EmailException(string message = "Error when mail sending") : base(message)
        {
        }
    }
}
