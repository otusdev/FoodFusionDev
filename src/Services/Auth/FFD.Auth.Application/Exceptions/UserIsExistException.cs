﻿namespace FFD.Auth.Application.Exceptions
{
    public class UserIsExistException : ApplicationException
    {
        public UserIsExistException(string message = "This user is exist") : base(message)
        {
        }
    }
}
