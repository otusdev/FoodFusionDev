﻿namespace FFD.Auth.Application.Exceptions
{
    public class NotPermissionException : ApplicationException
    {
        public NotPermissionException(string message = "This action need more hight permission") : base(message)
        {
        }
    }
}
