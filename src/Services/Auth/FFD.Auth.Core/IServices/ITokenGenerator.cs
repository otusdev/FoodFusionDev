﻿using FFD.Auth.Core.Entities.Db;
using FFD.Auth.Core.Settings;
using FFD.Auth.Core.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace FFD.Auth.Core.IServices
{
    public interface ITokenGenerator
    {
        string Generate(List<Claim> claims);
        bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt);
        RefreshToken GenerateRefreshToken();
        void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt);
        List<Claim> GenerateClaims(FFD.Auth.Core.Entities.Db.User user);
    }
}
