﻿using AutoMapper;
using FDD.Shared.Interfases;
using FFD.Courier.Core;
using FFD.Courier.Core.Entities.Db;
using FFD.Courier.Core.Entities.Dto;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFD.Courier.Application.CQRS.Chat.Command
{
    public class GetMyFinishOrdersQuery : IRequest<IEnumerable<OrderDto>>
    {     
    }
    public class GetMyFinishOrdersQueryHandler : IRequestHandler<GetMyFinishOrdersQuery, IEnumerable<OrderDto>>
    {
        private readonly IDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly IUserAccessor _userAccessor;
        public GetMyFinishOrdersQueryHandler(IDbContext dbContext, IMapper mapper, IUserAccessor userAccessor)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _userAccessor = userAccessor;
        }
        public async Task<IEnumerable<OrderDto>> Handle(GetMyFinishOrdersQuery request, CancellationToken cancellationToken)
        {
            var orders = _dbContext.Orders.Where(x => x.CourierId == _userAccessor.Id && x.Status =="Finished");           
            var result = _mapper.Map<IEnumerable<OrderDto>>(orders);
            return result;         
        }
    }
    public sealed class GetMyFinishOrdersQueryValidator : AbstractValidator<GetMyFinishOrdersQuery>
    {
    }
}
