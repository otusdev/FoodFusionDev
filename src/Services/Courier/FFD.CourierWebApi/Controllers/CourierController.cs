using FFD.Courier.Application.CQRS.Chat.Command;
using FFD.Courier.Core;
using FFD.Courier.Core.Entities.Db;
using FFD.Courier.Core.Entities.Dto;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace FFD.CourierWebApi.Controllers
{
    [ApiController, Authorize]
    [Route("[controller]")]
    public class CourierController : ControllerBase
    {    
        private readonly ILogger<CourierController> _logger;
        private readonly IMediator _mediator;
        public CourierController(ILogger<CourierController> logger, IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;           
        }

        [HttpPost("SendMessage")]
        public async Task<Response<object>> SendMessage(SendMessageCommand request, CancellationToken cancellationToken = default)
        {
            try
            {
                await _mediator.Send(request, cancellationToken);
            }
            catch (Exception ex)
            {
                return Response<object>.Fail(500, "Error SendMessage");
            }
            return Response<object>.Success(true);
        }

        [HttpPost("GetMessages")]
        public async Task<Response<object>> GetMessages(GetMessageQuery request, CancellationToken cancellationToken = default)
        {
            try
            {
                var result = await _mediator.Send(request, cancellationToken);
                return Response<object>.Success(result);
            }
            catch (Exception ex)
            {
                return Response<object>.Fail(500, "Error GetMessageQuery");
            }            
        }
        [HttpPost("AssignOrderToMe")]
        public async Task<Response<object>> AssignOrderToMe(AssignOrderToMeCommand request, CancellationToken cancellationToken = default)
        {
            try
            {
                await _mediator.Send(request, cancellationToken);
            }
            catch (Exception ex)
            {
                return Response<object>.Fail(500, "Error AssignOrderToMe");
            }
            return Response<object>.Success(true);
        }
        [HttpGet("GetMyOrders")]
        public async Task<Response<IEnumerable<OrderDto>>> GetMyOrders(CancellationToken cancellationToken = default)
        {
            IEnumerable<OrderDto> result;
            try
            {
                result = await _mediator.Send(new GetMyOrdersCommand(), cancellationToken);
            }
            catch (Exception ex)
            {
                return Response<IEnumerable<OrderDto>>.Fail(500, "Error GetMyOrders");
            }
            return Response<IEnumerable<OrderDto>>.Success(result);
        }

        [HttpGet("GetFinishOrders")]
        public async Task<Response<IEnumerable<OrderDto>>> GetFinishOrders(CancellationToken cancellationToken = default)
        {
            IEnumerable<OrderDto> result;
            try
            {
                result = await _mediator.Send(new GetMyFinishOrdersQuery(), cancellationToken);
            }
            catch (Exception ex)
            {
                return Response<IEnumerable<OrderDto>>.Fail(500, "Error GetMyOrders");
            }
            return Response<IEnumerable<OrderDto>>.Success(result);
        }

        [HttpGet("GetOrders")]
        public async Task<Response<List<Order>>> GetOrders(CancellationToken cancellationToken = default)
        {
            List<Order> result;
            try
            {
                result = await _mediator.Send(new GetOrdersCommand(), cancellationToken);
            }
            catch (Exception ex)
            {
                return Response<List<Order>>.Fail(500, "Error GetOrders");
            }
            return Response<List<Order>>.Success(result);
        }

        [HttpPost("FinishOrder")]
        public async Task<Response<bool>> FinishOrder(FinishOrderCommand request, CancellationToken cancellationToken = default)
        {
            bool result;
            try
            {
                result = await _mediator.Send(request, cancellationToken);
            }
            catch (Exception ex)
            {
                return Response<bool>.Fail(500, "Error GetOrders");
            }
            return Response<bool>.Success(result);
        }
    }
}
