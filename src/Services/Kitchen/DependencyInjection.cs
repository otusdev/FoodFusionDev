﻿using FDD.Shared.Interfases;
using FDD.Shared.Services;
using FFD.KitchenApi.DbLayer;
using FFD.KitchenApi.Notifie;
namespace FFD.KitchenApi
{

    public static class DependencyInjection
    {
        public static void InjectDependencies(this WebApplicationBuilder builder)
        {
            builder.Services.AddScoped<IDbContext, ApplicationDbContext>();
            builder.Services.AddScoped<IUserAccessor, UserAccessor>();
            builder.Services.AddScoped<Notifier>();
        }
    }
}
