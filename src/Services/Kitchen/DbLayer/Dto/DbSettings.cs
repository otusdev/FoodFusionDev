﻿namespace FFD.KitchenApi.DbLayer.Dto
{
    public class DbSettings
    {
        public string DefaultConnection { get; set; }
    }
}
