using FFD.KitchenApi.Listener;
using FFD.KitchenApi;
using FFD.KitchenApi.DbLayer;
using FFD.Shared.Extensions;
using FFD.Shared.Models.Settings;
using FFD.Shared.RabbitMq.Settings;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using FFD.KitchenApi.DbLayer.Dto;
using Microsoft.OpenApi.Models;
using Microsoft.Extensions.Options;
using FFD.Shared.RabbitMq;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddDbContext<ApplicationDbContext>(options =>
                                                       options.UseNpgsql(builder.Configuration.GetConnectionString("DefaultConnection")));
builder.Services.Configure<DbSettings>(builder.Configuration.GetSection("ConnectionStrings"));

//auth
var authenticationSection = builder.Configuration.GetSection("Authentication");
var authOptions = authenticationSection.Get<AuthOptions>();
builder.Services.Configure<AuthOptions>(authenticationSection);
var jwtOptions = JwtService.GetJwtBearerOptions(authOptions);
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
.AddJwtBearer(options =>
{
    options.RequireHttpsMetadata = jwtOptions.RequireHttpsMetadata;
    options.SaveToken = jwtOptions.SaveToken;
    options.TokenValidationParameters = jwtOptions.TokenValidationParameters;
});

// Add services to the container.
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c =>
{
    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
        In = ParameterLocation.Header,
        Description = "Please insert JWT with Bearer into field",
        Name = "Authorization",
        Type = SecuritySchemeType.ApiKey
    });

    c.AddSecurityRequirement(new OpenApiSecurityRequirement
        {
            {
                new OpenApiSecurityScheme
                {
                   Reference = new OpenApiReference
                   {
                     Type = ReferenceType.SecurityScheme,
                     Id = "Bearer"
                   }
                },
                new string[] { }
            }
        });
});

#region RabbitMq configure
builder.Services.Configure<SenderSettings>(builder.Configuration.GetSection("RabbitMq").GetSection("Sender"));
builder.Services.Configure<BrokerSettings>(builder.Configuration.GetSection("RabbitMq").GetSection("Broker"));
builder.Services.AddScoped<QueueSender>(provider =>
{
    var senderSettings = provider.GetRequiredService<IOptions<SenderSettings>>().Value;
    var brokerSettings = provider.GetRequiredService<IOptions<BrokerSettings>>().Value;
    return new QueueSender(senderSettings, brokerSettings);
});
#endregion

builder.Services.AddHttpContextAccessor();
//builder.Services.AddAutoMapper(typeof(Application));
builder.InjectDependencies();

//rabbit config
var brokerSettings = new BrokerSettings();
builder.Configuration.GetSection("RabbitMQ").GetSection("Broker").Bind(brokerSettings);
builder.Services.AddSingleton(brokerSettings);

var receiverSettings = new ReceiverSettings();
builder.Configuration.GetSection("RabbitMQ").GetSection("Listener").Bind(receiverSettings);
builder.Services.AddSingleton(receiverSettings);


builder.Services.AddHostedService<KitchenQueueListener>(_ => new KitchenQueueListener(brokerSettings, receiverSettings, _.GetRequiredService<IServiceScopeFactory>()));

var app = builder.Build();

//using (var scope = app.Services.CreateScope())
//{
//    var dbContext = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

//    //dbContext.Database.EnsureDeleted();
//    //dbContext.Database.EnsureCreated();
//}

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();