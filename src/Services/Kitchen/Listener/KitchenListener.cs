﻿using FFD.KitchenApi.DbLayer;
using FFD.KitchenApi.DbLayer.Model;
using FFD.Shared.Models;
using FFD.Shared.RabbitMq;
using FFD.Shared.RabbitMq.Settings;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System.Diagnostics;
using System.Text.Json;

namespace FFD.KitchenApi.Listener
{
    public class KitchenQueueListener : QueueListener
    {
        //private IDbContext _dbContext;
        //private DbContextOptions<ApplicationDbContext> _options;
        //private readonly IDbContextFactory<ApplicationDbContext> _contextFactory;
        private readonly IServiceScopeFactory _scopeFactory;

        public KitchenQueueListener(BrokerSettings brokerSettings, ReceiverSettings receiverSettings, IServiceScopeFactory scopeFactory) : base(brokerSettings, receiverSettings)
        {
            _scopeFactory = scopeFactory;
            //_dbContext = dbContext;    
            //_options = options;
            //_contextFactory = contextFactory;
        }

        protected override async Task<bool> ProcessMessageAsync(string message)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();
                var dbContext = scope.ServiceProvider.GetRequiredService<IDbContext>();
                var input = JsonSerializer.Deserialize<DishRabbit>(message);
                var exist = dbContext.Orders.FirstOrDefault(x => x.Id == input.OrderId);
                if (exist == null)
                {
                    var orderToSave = new Order()
                    {
                        User = JsonSerializer.Serialize(input.User),
                        Status = "New",
                        Id = input.OrderId,
                        Dishes = JsonSerializer.Serialize(input.ListDish)
                    };
                    dbContext.Orders.Add(orderToSave);
                    await dbContext.SaveChangesAsync();
                }                
               
                Debug.WriteLine("success recive " + message);
                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("error recive " + ex.Message);
                return false;
            }
            //.Ack(ea.DeliveryTag);
            // using var scope = _serviceProvider.CreateScope();
            //var orderRepository = scope.ServiceProvider.GetService<Order>();
        }
    }
}
